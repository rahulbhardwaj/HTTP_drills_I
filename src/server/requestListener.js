const http = require("http");
const { v4: uuidv4 } = require("uuid");
const fs = require("fs").promises;

const requestListener = (request, response) => {
  if (request.url === "/") {
    fs.readFile("src/data/welcome.html", "utf-8")
      .then((data) => {
        response.writeHead(200, { "Content-Type": "text/html" });
        response.write(data);
        return response.end();
      })
      .catch((error) => {
        console.log(error);
        response.writeHead(400, { "Content-Type": "text/plain" });
        response.write("Welcome to My HTTP Drills");
        return response.end();
      });
  } else if (request.url === "/html" && request.method === "GET") {
    fs.readFile("src/data/data.html", "utf-8")
      .then((data) => {
        response.writeHead(200, { "Content-Type": "text/html" });
        response.write(data);
        return response.end();
      })
      .catch((error) => {
        console.log(error);
        response.writeHead(500, { "Content-Type": "application/json" });
        response.write("Error while reading html file.");
        return response.end();
      });
  } else if (request.url === "/json" && request.method === "GET") {
    fs.readFile("src/data/data.json", "utf-8")
      .then((data) => {
        response.writeHead(200, { "Content-Type": "application/json" });
        response.write(data);
        return response.end();
      })
      .catch((error) => {
        console.log(error);
        response.writeHead(500, { "Content-Type": "application/json" });
        response.write("Error while reading Json file");
        return response.end();
      });
  } else if (request.url === "/uuid" && request.method === "GET") {
    response.writeHead(200, { "Content-Type": "application/json" });
    response.write(JSON.stringify({ uuid: uuidv4() }));
    return response.end();
  } else if (request.url.startsWith("/status/") && request.method === "GET") {
    try {
      const statusCode = request.url.split("/")[2];
      response.writeHead(200, { "Content-Type": "application/json" });
      response.write(`${statusCode} : ${http.STATUS_CODES[statusCode]}`);
      return response.end();
    } catch {
      response.writeHead(400, { "Content-Type": "application/json" });
      response.write("Wrong HTTP status code");
      return response.end();
    }
  } else if (request.url.startsWith("/delay/") && request.method === "GET") {
    const seconds = Number(request.url.split("/")[2]);
    if (isNaN(seconds)) {
      response.writeHead(400, { "Content-Type": "application/json" });
      response.write("Seconds should be a number");
      return response.end();
    } else if (seconds < 0) {
      response.writeHead(400, { "Content-Type": "application/json" });
      response.write("Seconds should be equal or greater than 0");
      return response.end();
    } else {
      setTimeout(() => {
        response.writeHead(200, { "Content-Type": "application/json" });
        response.write(`Sucess , delay in response is ${seconds} seconds.`);
        return response.end();
      }, seconds * 1000);
    }
  } else {
    response.writeHead(404, { "Content-Type": "application/json" });
    response.write("Wrong URL , Try Again");
    return response.end();
  }
};

module.exports = requestListener;