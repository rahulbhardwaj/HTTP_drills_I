const http = require("http");
const port = process.env.PORT || 3000;

const requestListener = require('./src/server/requestListener.js')

const server = http.createServer(requestListener);

server.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});